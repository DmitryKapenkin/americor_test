<?php

/**
 * @var $this yii\web\View
 * @var $model \app\models\History
 * @var $dataProvider \yii\data\ArrayDataProvider
 * @var $exportType string
 */

use app\widgets\Export\Export;

$filename = 'history';
$filename .= '-' . time();

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
?>

<?= Export::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'ins_ts',
            'label' => Yii::t('app', 'Date'),
            'format' => 'datetime'
        ],
        [
            'attribute' => 'user',
            'label' => Yii::t('app', 'User'),
        ],
        [
            'attribute' => 'type',
            'label' => Yii::t('app', 'Type'),
        ],
        [
            'attribute' => 'event',
            'label' => Yii::t('app', 'Event'),
        ],
        [
            'attribute' => 'message',
            'label' => Yii::t('app', 'Message'),
        ]
    ],
    'exportType' => $exportType,
    'batchSize' => 2000,
    'filename' => $filename
]) ?>
