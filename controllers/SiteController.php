<?php

namespace app\controllers;

use app\models\History;
use app\models\search\HistorySearch;
use app\widgets\HistoryList\decorators\DecoratorFactory;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\ErrorAction;

/**
 * Class SiteController - Main controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }


    /**
     * @param string $exportType
     *
     * @return string
     */
    public function actionExport($exportType): string
    {
        $model = new HistorySearch();
        $arDataProvider = $model->search(Yii::$app->request->queryParams);

        if (false !== $arDataProvider->getPagination()) {
            $arDataProvider->getPagination()->setPageSize(0);
        }

        $dataProvider = new ArrayDataProvider([
            'key' => 'id',
            'allModels' => array_map(static function (History $history) {
                return [
                    'id' => $history->id,
                    'ins_ts' => $history->ins_ts,
                    'user' => isset($history->user) ? $history->user->username : Yii::t('app', 'System'),
                    'type' => $history->object,
                    'event' => $history->eventText,
                    'message' => strip_tags(DecoratorFactory::getDecorator($history)->getBody($history)),
                ];
            }, $arDataProvider->models),
            'sort' => $arDataProvider->getSort(),
            'pagination' => false,
        ]);

        return $this->render('export', [
            'dataProvider' => $dataProvider,
            'exportType' => $exportType,
            'model' => $model
        ]);
    }
}
