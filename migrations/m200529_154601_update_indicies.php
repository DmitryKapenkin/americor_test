<?php

use app\models\History;
use yii\db\Migration;

/**
 * Class m200529_154601_update_indicies
 */
class m200529_154601_update_indicies extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $this->createIndex('ins_ts', History::tableName(), 'ins_ts');
        $this->createIndex('object', History::tableName(), 'object');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->dropIndex('ins_ts', History::tableName());
        $this->dropIndex('object', History::tableName());

        echo 'm200529_154601_update_indicies cannot be reverted.' . PHP_EOL;

        return true;
    }
}
