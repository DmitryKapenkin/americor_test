# Тестовое задание компании Americor на должность «Senior PHP developer»

Дана часть кода из проекта. Решение вашего задания мы нигде не сможем использовать, у нас эта проблема уже решена.  
Учтите, что проект будет расти, количество событий и объектов увеличиваться. 150+ событий, рост около 2 событий в месяц. 30+ объектов.

**Сделайте следующее:**

1. Импортирйте код в свой git-репозиторий.
2. В отдельной ветке проведите рефакторинг вывода ленты истории и экспорта данных.
3. Сделайте pull request из ветки в master. В описании pull request:  
   - Приведите списком произведённые изменения.  
   - Для каждого изменения опишите, для чего было сделано.  
4. Пришлите нам ссылку на pull request.

**При рассмотрении решения хотим увидеть:**

1. Понимание ООП.
2. Умение применять паттерны проектирования.
3. Умение разделять код.
4. Понимание Yii 2.
5. Какие проблемы вы считаете первостепенными.
6. Насколько ваш подход будет удобен в поддержке и развитии.

## Изменения в проекте

Добавлен генератор для заполнения таблицы history дополнительными данными

`php yii history-generator N`, где N - количество сгенерированнх записей

### Общее ревью кода:
1. Использование строгой типизации для передваемых параметров и возвращаемых значений
2. Общий стиль кода приведен к единому виду
3. Исправление опечаток.

### Оптимизации
1. Добавлены индексы в таблицу history для оптимизации поисковых запросов.
2. Изменен запрос (использован подзапрос с ограничением на общий лимит выбираемых записей).
Из моего опыта крайне редко требуется выводить в таблице все записи, а их в этой таблице могут быть миллионы.
Данный подход может немного проигрывать в скорости выполнения запроса, если выборка данных идет для первых страниц и с применением фильтров, однако при просмотре последних страниц выборки по скорости может быть эффективнее в 2-3 раза.

### Изменения в структуре

Для отображения данных в таблице использованы декораторы, в которые был вынесен код, формирующий набор данных для рендеринга.
В идеале, необходимо переработать текущие модели AR, полностью убрав из них код, отвечающий за формирование текстов на основе тех или иных значений в модели. Сами модели должны быть предельно простыми, вся логика их обработки и вывода должна располагаться в отдельных классах.

### Тестирование кода

В рамках изменений написан простой тестовый класс для демонстрации возможности работы с тестами
Для запуска нужно использовать команду `vendor/bin/codecept run`

В идеале, необходимо покрыть тестами Модели и Декораторы с использованием тестового набора данных (это может быть провайдер данных, как в примере кода, либо полноценная тестовая база), однако лимит времени на выполнение тестового задания был ограничен.
