<?php

namespace app\commands;

use app\models\Call;
use app\models\Customer;
use app\models\Event;
use app\models\Fax;
use app\models\History;
use app\models\Sms;
use app\models\Task;
use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;

class HistoryGeneratorController extends Controller
{
    public function actionIndex($limit)
    {
        $attr = [];
        $userId = ArrayHelper::getColumn(User::find()->select('id')->all(), 'id');
        $objectIds = [
            Call::class => ArrayHelper::getColumn(Call::find()->select('id')->all(), 'id'),
            Fax::class => ArrayHelper::getColumn(Fax::find()->select('id')->all(), 'id'),
            Sms::class => ArrayHelper::getColumn(Sms::find()->select('id')->all(), 'id'),
            Task::class => ArrayHelper::getColumn(Task::find()->select('id')->all(), 'id'),
            Customer::class => ArrayHelper::getColumn(Customer::find()->select('id')->all(), 'id'),
        ];
        $object = [
            Call::class => 'call',
            Fax::class => 'fax',
            Sms::class => 'sms',
            Task::class => 'task',
            Customer::class => 'customer',
        ];
        $events = Event::getEventList();
        $columns = [
            'user_id',
            'customer_id',
            'event',
            'object',
            'object_id',
        ];

        for (; $limit > 0; $limit--) {
            $objectClass = array_rand($events);
            $attr[] = [
                'user_id' => $userId[array_rand($userId)],
                'customer_id' => $objectIds[Customer::class][array_rand($objectIds[Customer::class])],
                'event' => $events[$objectClass][array_rand($events[$objectClass])],
                'object' => $object[$objectClass],
                'object_id' => $objectIds[$objectClass][array_rand($objectIds[$objectClass])],
            ];

            if (0 === $limit % 2) {
                \Yii::$app->db->createCommand()->batchInsert(History::tableName(), $columns, $attr)->execute();
                $attr = [];
            }
        }

        if (!empty($attr)) {
            \Yii::$app->db->createCommand()->batchInsert(History::tableName(), $columns, $attr)->execute();
        }

        return ExitCode::OK;
    }
}
