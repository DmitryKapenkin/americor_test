<?php

namespace tests\unit\widgets\HistotyList\decorators;

use app\models\History;
use app\models\User;
use app\widgets\HistoryList\decorators\DefaultDecorator;
use Codeception\Test\Unit;

/**
 * Class DefaultDecoratorTest
 */
class DefaultDecoratorTest extends Unit
{
    /**
     * @dataProvider dataProvider
     * @param array $attr
     */
    public function testMain(array $attr)
    {
        $user = $this->createMock(User::class);
        $user
            ->expects($this->once())
            ->method('__get')
            ->with('id')
            ->willReturn($attr['user_id'])
        ;
        $history = $this->createMock(History::class);
        $history
            ->expects($this->exactly(3))
            ->method('__get')
            ->withConsecutive(['user'], ['eventText'], ['ins_ts'])
            ->willReturnOnConsecutiveCalls($user, $attr['event'], $attr['ins_ts'])
        ;

        $decorator = new DefaultDecorator();
        $viewData = $decorator->getViewData($history);
        $this->assertEquals($attr['user_id'], $viewData['user']->id);
        $this->assertEquals($attr['event'], $viewData['body']);
        $this->assertEquals($attr['ins_ts'], $viewData['bodyDatetime']);
        $this->assertEquals('fa-gear bg-purple-light', $viewData['iconClass']);
    }

    /**
     * @return \array[][]
     * @throws \Exception
     */
    public function dataProvider(): array
    {
        return [
            [[
                'user_id' => random_int(1, 1000),
                'event' => 'Task created',
                'ins_ts' => (new \DateTime('2000-01-01'))->format('Y-m-d H:i:s'),
            ]],
            [[
                'user_id' => random_int(1, 1000),
                'event' => 2,
                'ins_ts' => (new \DateTime('2000-01-01'))->format('Y-m-d H:i:s'),
            ]],
        ];
    }
}
