<?php

namespace app\models;

/**
 * Class Event
 */
class Event
{
    public const EVENT_CREATED_TASK = 'created_task';
    public const EVENT_UPDATED_TASK = 'updated_task';
    public const EVENT_COMPLETED_TASK = 'completed_task';

    public const EVENT_INCOMING_SMS = 'incoming_sms';
    public const EVENT_OUTGOING_SMS = 'outgoing_sms';

    public const EVENT_INCOMING_CALL = 'incoming_call';
    public const EVENT_OUTGOING_CALL = 'outgoing_call';

    public const EVENT_INCOMING_FAX = 'incoming_fax';
    public const EVENT_OUTGOING_FAX = 'outgoing_fax';

    public const EVENT_CUSTOMER_CHANGE_TYPE = 'customer_change_type';
    public const EVENT_CUSTOMER_CHANGE_QUALITY = 'customer_change_quality';

    public static function getEventList(): array
    {
        return [
            Task::class => [
                self::EVENT_CREATED_TASK,
                self::EVENT_UPDATED_TASK,
                self::EVENT_COMPLETED_TASK,
            ],
            Sms::class => [
                self::EVENT_INCOMING_SMS,
                self::EVENT_OUTGOING_SMS,
            ],
            Call::class => [
                self::EVENT_INCOMING_CALL,
                self::EVENT_OUTGOING_CALL,
            ],
            Fax::class => [
                self::EVENT_INCOMING_FAX,
                self::EVENT_OUTGOING_FAX,
            ],
            Customer::class => [
                self::EVENT_CUSTOMER_CHANGE_TYPE,
                self::EVENT_CUSTOMER_CHANGE_QUALITY,
            ],
        ];
    }
}
