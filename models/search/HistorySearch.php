<?php

namespace app\models\search;

use app\models\History;
use app\widgets\HistoryList\HistoryList;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * HistorySearch represents the model behind the search form about `app\models\History`.
 */
class HistorySearch extends History
{
    private const TIME_SUFFIX = ' 00:00:00';
    public $date_range;
    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [[
                'customer_id',
                'object',
                'user_id',
                'search',
                'department_ids',
                'date_range',
                'date_from',
                'date_to',
                'denyObjects'
            ], 'safe'],
            [['date_from', 'date_to'], 'date', 'format' => 'Y-MM-dd'],
        ];

    }

    public function behaviors(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'customer_id' => Yii::t('app', 'Customers'),
            'user_id' => Yii::t('app', 'Agents'),
            'object' => Yii::t('app', 'Types'),
            'search' => Yii::t('app', 'Search'),
            'department_ids' => Yii::t('app', 'Department'),
            'date_range' => Yii::t('app', 'Date Interval'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = History::find();
        $subQuery = History::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'ins_ts' => SORT_DESC,
                'id' => SORT_DESC,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->addSelect('history.*');
        $query->with([
            'customer',
            'user',
            'sms',
            'task',
            'call',
            'fax',
        ]);

        $sort = $dataProvider->getSort();
        $orderBy = [];

        if ($sort) {
            $orderBy = $sort->getAttributeOrders();
        }

        if ($this->date_from && $this->date_to) {
            $subQuery->where([
                'between',
                'ins_ts',
                $this->date_from . self::TIME_SUFFIX,
                $this->date_to . self::TIME_SUFFIX,
            ]);
        }

        $subQuery
            ->addSelect('id')
            ->andFilterWhere([
                'history.customer_id' => $this->customer_id,
                'history.user_id' => $this->user_id,
                'history.object' => $this->object,
            ])
            ->orderBy($orderBy)
            ->limit(HistoryList::LIMIT)
        ;

        $query->innerJoin(['subQ' => $subQuery], 'history.id = subQ.id');

        return $dataProvider;
    }
}
