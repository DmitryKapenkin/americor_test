<?php

use app\models\search\HistorySearch;
use kartik\daterange\DateRangePicker;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\icons\FontAwesomeAsset;
use kartik\select2\Select2;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider ActiveDataProvider */
/* @var $model HistorySearch */
/* @var $linkExport string */
/* @var $customers array */
/* @var $users array */
/* @var $objects array */

FontAwesomeAsset::register($this);
?>

<?php Pjax::begin(['id' => 'grid-pjax', 'formSelector' => false]); ?>

<div class="panel panel-primary panel-small m-b-0">
    <div class="panel-body panel-body-selected">
        <div class="pull-sm-right">
            <?php if (!empty($linkExport)): ?>
                <?= Html::a(
                    Yii::t('app', 'CSV'),
                    $linkExport,
                    [
                        'class' => 'btn btn-success',
                        'data-pjax' => 0,
                        'target' => '_blank',
                    ]
                ) ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="search-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date_range', [
        'addon' => ['prepend' => ['content' => '<i class="fas fa-calendar-alt"></i>']],
        'options' => ['class' => 'drp-container form-group'],
    ])->widget(DateRangePicker::class, [
        'useWithAddon' => true,
        'startAttribute' => 'date_from',
        'endAttribute' => 'date_to',
        'pluginOptions' => [
            'locale' => ['format' => 'YYYY-MM-DD'],
        ],
    ]) ?>

    <?= $form->field($model, 'customer_id')->widget(Select2::class, [
        'data' => $customers,
        'options' => ['placeholder' => 'Select a customer ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'user_id')->widget(Select2::class, [
        'data' => $users,
        'options' => ['placeholder' => 'Select a user ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'object')->widget(Select2::class, [
        'data' => $objects,
        'options' => ['placeholder' => 'Select a type ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Apply', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Clear', ['index'], ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php if ($dataProvider->totalCount !== 0): ?>
<div class="alert alert-info" role="alert">
    The first 10000 results are shown. Use filters to find the information you need.
</div>
<?php endif; ?>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'options' => [
        'tag' => 'ul',
        'class' => 'list-group'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'list-group-item'
    ],
    'emptyTextOptions' => ['class' => 'empty p-20'],
    'layout' => '{items}{pager}',
]) ?>

<?php Pjax::end(); ?>
