<?php
/** @var $this View */
/** @var $model HistorySearch */
use app\widgets\HistoryList\decorators\DecoratorFactory;
use app\models\search\HistorySearch;
use yii\web\View;

$decorator = DecoratorFactory::getDecorator($model);
echo $this->render($decorator->getViewName(), $decorator->getViewData($model));
