<?php

namespace app\widgets\HistoryList\decorators;

use app\models\History;;

/**
 * Class DefaultDecorator
 */
class DefaultDecorator extends BaseDecorator
{
    /**
     * {@inheritdoc}
     */
    public function getViewName(): string
    {
        return '_item_common';
    }

    /**
     * {@inheritdoc}
     */
    public function getViewData(History $history): array
    {
        return [
            'user' => $history->user,
            'body' => $history->eventText,
            'bodyDatetime' => $history->ins_ts,
            'iconClass' => 'fa-gear bg-purple-light',
        ];
    }
}
