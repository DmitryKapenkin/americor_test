<?php

namespace app\widgets\HistoryList\decorators;

use app\models\History;
use yii\db\ActiveRecord;

/**
 * Class BaseDecorator
- */
abstract class BaseDecorator implements DecoratorInterface
{
    /**
     * @var string
     */
    protected $relField;

    /**
     * @param History $history
     *
     * @return ActiveRecord|null
     */
    public function getRelationModel(History $history): ?ActiveRecord
    {
        return $history->{$this->relField} ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public function getBody(History $history): ?string
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getFooter(History $history): ?string
    {
        return null;
    }
}
