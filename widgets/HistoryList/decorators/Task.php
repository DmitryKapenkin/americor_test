<?php

namespace app\widgets\HistoryList\decorators;

use app\models\History;

/**
 * Class Task
 */
class Task extends BaseDecorator
{
    /**
     * {@inheritdoc}
     */
    protected $relField = 'task';

    /**
     * {@inheritdoc}
     */
    public function getViewData(History $history): array
    {
        /** @var \app\models\Task $relModel */
        $relModel = $this->getRelationModel($history);

        return [
            'user' => $history->user,
            'body' => $this->getBody($history),
            'iconClass' => 'fa-check-square bg-yellow',
            'footerDatetime' => $history->ins_ts,
            'footer' => isset($relModel->customerCreditor->name) ? "Creditor: " . $relModel->customerCreditor->name : ''
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getViewName(): string
    {
        return '_item_common';
    }

    /**
     * {@inheritdoc}
     */
    public function getBody(History $history): ?string
    {
        /** @var \app\models\Task $relModel */
        $relModel = $this->getRelationModel($history);

        return $history->eventText . ': ' . ($relModel->title ?? '');
    }
}
