<?php

namespace app\widgets\HistoryList\decorators;

use app\models\History;
use yii\helpers\Html;

/**
 * Class Fax
 * @property \app\models\Fax $relModel
 */
class Fax extends BaseDecorator
{
    /**
     * {@inheritdoc}
     */
    protected $relField = 'fax';

    /**
     * {@inheritdoc}
     */
    public function getViewData(History $history): array
    {
        return [
            'user' => $history->user,
            'body' => $this->getBody($history),
            'footer' => $this->getFooter($history),
            'footerDatetime' => $history->ins_ts,
            'iconClass' => 'fa-fax bg-green'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getViewName(): string
    {
        return '_item_common';
    }

    /**
     * {@inheritdoc}
     */
    public function getBody(History $history): ?string
    {
        /** @var \app\models\Fax|null $relModel */
        $relModel = $this->getRelationModel($history);

        return $history->eventText .
            ' - ' .
            (null !== $relModel && isset($relModel->document) ? Html::a(
                \Yii::t('app', 'view document'),
                $relModel->document->getViewUrl(),
                [
                    'target' => '_blank',
                    'data-pjax' => 0
                ]
            ) : '');
    }

    /**
     * {@inheritdoc}
     */
    public function getFooter(History $history): ?string
    {
        /** @var \app\models\Fax|null $relModel */
        $relModel = $this->getRelationModel($history);

        return \Yii::t('app', '{type} was sent to {group}', [
            'type' => $relModel ? $this->getTypeText((string) $relModel->type) : 'Fax',
            'group' => null !== $relModel && isset($relModel->creditorGroup) ?
                Html::a($relModel->creditorGroup->name, ['creditors/groups'], ['data-pjax' => 0])
                : ''
        ]);
    }

    /**
     * @return array
     */
    public static function getTypeTexts(): array
    {
        return [
            \app\models\Fax::TYPE_POA_ATC => \Yii::t('app', 'POA/ATC'),
            \app\models\Fax::TYPE_REVOCATION_NOTICE => \Yii::t('app', 'Revocation'),
        ];
    }

    /**
     * @param string $type
     *
     * @return string
     */
    public function getTypeText(string $type): string
    {
        return self::getTypeTexts()[$type] ?? $type;
    }
}
