<?php

namespace app\widgets\HistoryList\decorators;

use app\models\History;

class DecoratorFactory
{
    /**
     * @param History $mainRecord
     *
     * @return DecoratorInterface
     */
    public static function getDecorator(History $mainRecord): DecoratorInterface
    {
        switch ($mainRecord->event) {
            case History::EVENT_CREATED_TASK:
            case History::EVENT_COMPLETED_TASK:
            case History::EVENT_UPDATED_TASK:
                $decorator = new Task();
                break;
            case History::EVENT_INCOMING_SMS:
            case History::EVENT_OUTGOING_SMS:
                $decorator = new Sms();
                break;
            case History::EVENT_OUTGOING_FAX:
            case History::EVENT_INCOMING_FAX:
                $decorator = new Fax();
                break;
            case History::EVENT_CUSTOMER_CHANGE_TYPE:
            case History::EVENT_CUSTOMER_CHANGE_QUALITY:
                $decorator = new Customer();
                break;
            case History::EVENT_INCOMING_CALL:
            case History::EVENT_OUTGOING_CALL:
                $decorator = new Call();
                break;
            default:
                $decorator = new DefaultDecorator();
                break;
        }

        return $decorator;
    }
}
