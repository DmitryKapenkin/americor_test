<?php

namespace app\widgets\HistoryList\decorators;

use app\models\History;

/**
 * Class Call
 * @property \app\models\Call $relModel
 */
class Call extends BaseDecorator
{
    /**
     * {@inheritdoc}
     */
    protected $relField = 'call';

    /**
     * {@inheritdoc}
     */
    public function getViewData(History $history): array
    {
        /** @var \app\models\Call|null $relModel */
        $relModel = $this->getRelationModel($history);
        $answered = null !== $relModel ? $relModel->status === \app\models\Call::STATUS_ANSWERED : \app\models\Call::STATUS_NO_ANSWERED;

        return [
            'user' => $history->user,
            'content' => $relModel->comment ?? '',
            'body' => $this->getBody($history),
            'footerDatetime' => $history->ins_ts,
            'footer' => $this->getFooter($history),
            'iconClass' => $answered ? 'md-phone bg-green' : 'md-phone-missed bg-red',
            'iconIncome' => $answered && $relModel->direction === \app\models\Call::DIRECTION_INCOMING
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getViewName(): string
    {
        return '_item_common';
    }

    /**
     * @param History $history
     *
     * @return string
     */
    public function getBody(History $history): ?string
    {
        /** @var \app\models\Call|null $relModel */
        $relModel = $this->getRelationModel($history);

        return null !== $relModel ?
            $relModel->totalStatusText . (
            $relModel->getTotalDisposition(false) ?
                    " <span class='text-grey'>" . $relModel->getTotalDisposition(false) . "</span>"
                    : ""
            )
            : '<i>Deleted</i> ';
    }

    /**
     * @param History $history
     *
     * @return string|null
     */
    public function getFooter(History $history): ?string
    {
        /** @var \app\models\Call|null $relModel */
        $relModel = $this->getRelationModel($history);

        return null !== $relModel && isset($relModel->applicant) ? "Called <span>{$relModel->applicant->name}</span>" : null;
    }
}
