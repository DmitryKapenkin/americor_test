<?php

namespace app\widgets\HistoryList\decorators;

use app\models\History;

/**
 * Class Customer
 */
class Customer extends BaseDecorator
{
    /**
     * @inheritDoc
     */
    public function getViewName(): string
    {
        return '_item_statuses_change';
    }

    /**
     * @inheritDoc
     */
    public function getViewData(History $history): array
    {
        switch ($history->event) {
            case History::EVENT_CUSTOMER_CHANGE_TYPE:
                $viewData = [
                    'model' => $history,
                    'oldValue' => self::getTypeTextByType($history->getDetailOldValue('type')),
                    'newValue' => self::getTypeTextByType($history->getDetailNewValue('type'))
                ];
                break;
            case History::EVENT_CUSTOMER_CHANGE_QUALITY:
                $viewData = [
                    'model' => $history,
                    'oldValue' => self::getQualityTextByQuality($history->getDetailOldValue('quality')),
                    'newValue' => self::getQualityTextByQuality($history->getDetailNewValue('quality')),
                ];
                break;
            default:
                $viewData = [];
        }

        return $viewData;
    }

    /**
     * @inheritDoc
     */
    public function getBody(History $history): ?string
    {
        switch ($history->event) {
            case History::EVENT_CUSTOMER_CHANGE_TYPE:
                $body = "$history->eventText " .
                    (self::getTypeTextByType($history->getDetailOldValue('type')) ?? 'not set') . ' to ' .
                    (self::getTypeTextByType($history->getDetailNewValue('type')) ?? 'not set');
                break;
            case History::EVENT_CUSTOMER_CHANGE_QUALITY:
                $body = "$history->eventText " .
                    (self::getQualityTextByQuality($history->getDetailOldValue('quality')) ?? 'not set') . ' to ' .
                    (self::getQualityTextByQuality($history->getDetailNewValue('quality')) ?? 'not set');
                break;
            default:
                $body = null;
        }

        return $body;
    }



    /**
     * @return array
     */
    public static function getQualityTexts(): array
    {
        return [
            \app\models\Customer::QUALITY_ACTIVE => \Yii::t('app', 'Active'),
            \app\models\Customer::QUALITY_REJECTED => \Yii::t('app', 'Rejected'),
            \app\models\Customer::QUALITY_COMMUNITY => \Yii::t('app', 'Community'),
            \app\models\Customer::QUALITY_UNASSIGNED => \Yii::t('app', 'Unassigned'),
            \app\models\Customer::QUALITY_TRICKLE => \Yii::t('app', 'Trickle'),
        ];
    }

    /**
     * @param string|null $quality
     *
     * @return string|null
     */
    public static function getQualityTextByQuality(?string $quality = null): ?string
    {
        return self::getQualityTexts()[$quality] ?? $quality;
    }

    /**
     * @return array
     */
    public static function getTypeTexts(): array
    {
        return [
            \app\models\Customer::TYPE_LEAD => \Yii::t('app', 'Lead'),
            \app\models\Customer::TYPE_DEAL => \Yii::t('app', 'Deal'),
            \app\models\Customer::TYPE_LOAN => \Yii::t('app', 'Loan'),
        ];
    }

    /**
     * @param string|null $type
     *
     * @return string|null
     */
    public static function getTypeTextByType(?string $type = null): ?string
    {
        return self::getTypeTexts()[$type] ?? $type;
    }
}
