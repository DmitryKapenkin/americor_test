<?php

namespace app\widgets\HistoryList\decorators;

use app\models\History;

/**
 * Class Sms
 * @property \app\models\Sms $relModel
 */
class Sms extends BaseDecorator
{
    /**
     * {@inheritdoc}
     */
    protected $relField = 'sms';

    /**
     * {@inheritdoc}
     */
    public function getViewData(History $history): array
    {
        /** @var \app\models\Sms $relModel */
        $relModel = $this->getRelationModel($history);

        return [
            'user' => $history->user,
            'body' => $this->getBody($history),
            'footer' => $this->getFooter($history),
            'iconIncome' => null !== $relModel ? $relModel->direction === \app\models\Sms::DIRECTION_INCOMING : \app\models\Sms::STATUS_DRAFT,
            'footerDatetime' => $history->ins_ts,
            'iconClass' => 'icon-sms bg-dark-blue'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getViewName(): string
    {
        return '_item_common';
    }

    /**
     * {@inheritdoc}
     */
    public function getBody(History $history): ?string
    {
        /** @var \app\models\Sms $relModel */
        $relModel = $this->getRelationModel($history);

        return $relModel->message ?? '';
    }

    /**
     * {@inheritdoc}
     */
    public function getFooter(History $history): ?string
    {
        /** @var \app\models\Sms $relModel */
        $relModel = $this->getRelationModel($history);

        return null !== $relModel ? (
            $relModel->direction === \app\models\Sms::DIRECTION_INCOMING ?
                \Yii::t('app', 'Incoming message from {number}', [
                    'number' => $relModel->phone_from ?? ''
                ]) : \Yii::t('app', 'Sent message to {number}', [
                    'number' => $relModel->phone_to ?? ''
                ])
        ) : '';
    }
}
