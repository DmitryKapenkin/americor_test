<?php

namespace app\widgets\HistoryList\decorators;

use app\models\History;
use yii\db\ActiveRecord;

/**
 * Interface DecoratorInterface
 */
interface DecoratorInterface
{
    /**
     * Should return path to view for render
     *
     * @return string
     */
    public function getViewName(): string;

    /**
     * Should returns array with data for render
     *
     * @return array
     */
    public function getViewData(History $history): array;

    /**
     * @param History $history
     *
     * @return string|null
     */
    public function getBody(History $history): ?string;

    /**
     * @param History $history
     *
     * @return string|null
     */
    public function getFooter(History $history): ?string;

    /**
     * @param History $history
     *
     * @return ActiveRecord|null
     */
    public function getRelationModel(History $history): ?ActiveRecord;
}
