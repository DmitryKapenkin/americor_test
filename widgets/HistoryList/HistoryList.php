<?php

namespace app\widgets\HistoryList;

use app\models\Customer;
use app\models\History;
use app\models\search\HistorySearch;
use app\models\User;
use app\widgets\Export\Export;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Yii;

class HistoryList extends Widget
{
    /**
     * Limit rows for rendering
     */
    public const LIMIT = 10000;

    /**
     * @return string
     */
    public function run()
    {
        $model = new HistorySearch();

        return $this->render('main', [
            'model' => $model,
            'linkExport' => $this->getLinkExport(),
            'dataProvider' => $model->search(Yii::$app->request->bodyParams),
            /*
             * @todo Better change to ajax for loading list of Customer & User
             */
            'customers' => ArrayHelper::map(Customer::find()->all(), 'id', 'name'),
            'users' => ArrayHelper::map(User::find()->all(), 'id', static function (User $user) {
                $res = $user->username;

                if (User::STATUS_ACTIVE !== $user->status) {
                    $res .= ' (inactive)';
                }

                return $res;
            }),
            'objects' => array_combine(History::AVAILABLE_TYPES, History::AVAILABLE_TYPES),
        ]);
    }

    /**
     * @return string
     */
    private function getLinkExport()
    {
        $params = Yii::$app->getRequest()->getQueryParams();
        $params = ArrayHelper::merge([
            'exportType' => Export::FORMAT_CSV
        ], $params);
        $params[0] = 'site/export';

        return Url::to($params);
    }
}
